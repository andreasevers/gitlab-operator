# Updating Dependencies

### Updating the Kubernetes API client

The kubebuilder libraries tend to lag behind the K8s releases by several versions, so we manually adjust the library
versions in `Gopkg.toml`

We specify the version of `sigs.k8s.io/controller-runtime` and `sigs.k8s.io/controller-tools` that
supports the version of the API we want to support, and also update the k8s client/api versions.

I want to support Kubernetes 1.13, 1.14, and 1.15, we would target the 1.14 API. Checking https://github.com/kubernetes-sigs/controller-runtime/releases for the last controller runtime version using 1.14, and https://github.com/kubernetes-sigs/controller-tools/releases for the last controller-tools using 1.14.

> **Note:** In this scenario, we picked 1.10 due to the the [Kubernetes Supported Version Skew](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/release/versioning.md#supported-releases-and-component-skew)
> which says that clients must be within +1/-1 minor version of the master node.

Then we would update `Gopkg.toml`, setting the `sigs.k8s.io/controller-runtime` and `sigs.k8s.io/controller-tools` to those versions.
Then update the api components in `Gopkg.toml` to `kubernetes-1.14.1`. (These components can be identified by their previous version matching the same syntaxs)

Then running the following commands:

```
rm -rf vendor
make test
```

See the [kubebuilder documentation](http://kubebuilder.netlify.com/beyond_basics/upgrading_kubebuilder.html) for additional information.
