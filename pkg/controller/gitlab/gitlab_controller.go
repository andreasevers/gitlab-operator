/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/ghodss/yaml"

	gitlabv1beta1 "gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/apis/gitlab/v1beta1"
	"gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/version"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	sharedSecretsLabelSelector     = "app=shared-secrets"
	webserviceLabelSelector        = "app in (unicorn, webservice)"
	gitlabShellLabelSelector       = "app=gitlab-shell"
	sidekiqLabelSelector           = "app=sidekiq"
	taskRunnerLabelSelector        = "app=task-runner"
	gitalyLabelSelector            = "app=gitaly"
	migrationsLabelSelector        = "app=migrations"
	gitlabVersionAnnotationKey     = "gitlab.com/version"
	gitlabLastRestartAnnotationKey = "gitlab.com/last-restart"
	gitlabOperatorNameLabelKey     = "gitlab.com/operator-name"
	gitlabOperatorNameLabelValue   = "gitlab-operator"
	timeFormat                     = "20060102150405"
	defaultJobParallelism          = 1
	helmLabelSelector              = "release"
	gitalyRollingUpdateTimeout     = 300 * time.Second
	gitalyRollingUpdateCheckPeriod = 5 * time.Second
)

var gitlabClassToWatch = ""

// Add creates a new GitLab Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileGitLab{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// Finds or infers gitlab class from an object. Returns empty string if it is missing.
func findGitLabClass(obj metav1.Object) string {
	gitlabClass, _ := obj.GetAnnotations()["gitlab.com/class"]
	return gitlabClass
}

// Checks if the gitlab class of the object matches the class to be watched. If they
// don't match the object must be ignored.
func isMatchingGitLabClass(obj metav1.Object) bool {
	gitlabClass := findGitLabClass(obj)
	return gitlabClass != "" && gitlabClass == gitlabClassToWatch
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("gitlab-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	gitlabClassToWatch = os.Getenv("GITLAB_CLASS")

	// Only process gitlab objects with the appropriate version label and class annotation
	gitlabPredicate := predicate.Funcs{
		UpdateFunc: func(e event.UpdateEvent) bool {
			versionLabel, _ := e.MetaNew.GetLabels()["controller.gitlab.com"]
			if versionLabel != version.GetVersion() || !isMatchingGitLabClass(e.MetaNew) {
				return false
			}

			// Don't reconcile meta or status updates
			if e.MetaNew.GetGeneration() == e.MetaOld.GetGeneration() {
				return false
			}

			return e.ObjectOld != e.ObjectNew
		},
		CreateFunc: func(e event.CreateEvent) bool {
			versionLabel, _ := e.Meta.GetLabels()["controller.gitlab.com"]
			return versionLabel == version.GetVersion() && isMatchingGitLabClass(e.Meta)
		},
		DeleteFunc: func(e event.DeleteEvent) bool {
			versionLabel, _ := e.Meta.GetLabels()["controller.gitlab.com"]
			return versionLabel == version.GetVersion() && isMatchingGitLabClass(e.Meta)
		},
		GenericFunc: func(e event.GenericEvent) bool {
			versionLabel, _ := e.Meta.GetLabels()["controller.gitlab.com"]
			return versionLabel == version.GetVersion() && isMatchingGitLabClass(e.Meta)
		},
	}

	// Watch for changes to GitLab
	err = c.Watch(&source.Kind{Type: &gitlabv1beta1.GitLab{}}, &handler.EnqueueRequestForObject{}, gitlabPredicate)
	if err != nil {
		return err
	}

	// TODO: watch for all managed resources (deployments, statefulsets etc.) and trigger requeue when any changes are observed

	return nil
}

var _ reconcile.Reconciler = &ReconcileGitLab{}

// ReconcileGitLab reconciles a GitLab object
type ReconcileGitLab struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a GitLab object and makes changes based on the state read
// and what is in the GitLab.Spec
// Automatically generate RBAC rules to allow the Controller to read and write Deployments, StatefulSets and Jobs
// +kubebuilder:rbac:groups=apps,resources=deployments;statefulsets;daemonsets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=roles;rolebindings;clusterroles;clusterrolebindings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitlab.com,resources=gitlabs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=pods;configmaps;secrets;serviceaccounts,verbs=get;list;watch;create;update;patch;delete
func (r *ReconcileGitLab) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the GitLab instance
	gitlab := &gitlabv1beta1.GitLab{}

	err := r.Get(context.TODO(), request.NamespacedName, gitlab)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Pause all workloads before unpausing the shared-secrets Job to allow the
	// operator to control the update if required
	log.Print("Pausing all workloads")
	err = r.pauseWorkloads(gitlab, true)
	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error: %s", err))
	}

	// Pause gitaly, this is done separately so it can be unpaused and updated separately from other services
	log.Print("Pausing Gitaly")
	err = r.pauseStatefulSets(gitlab, gitalyLabelSelector, true)
	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error pausing Gitaly: %s", err))
	}

	log.Printf("Updating")

	log.Print("Running shared-secrets")
	sharedSecretsJobName, selfSignedCertJobName, err := r.runSharedSecrets(gitlab)
	r.cleanUpSharedSecrets(gitlab, sharedSecretsJobName, selfSignedCertJobName)

	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error: %s", err))
	}

	log.Print("Running pre migrations")
	migrationsJobName, err := r.runMigrations(gitlab, "pre")
	r.cleanUpMigrations(gitlab, migrationsJobName)
	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error: %s", err))
	}

	log.Print("Rolling Gitaly")
	err = r.rollingUpdateGitaly(gitlab)
	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error rolling Gitaly: %s", err))
	}

	log.Print("Unpausing all workloads")
	err = r.pauseWorkloads(gitlab, false)
	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error unpausing workloads: %s", err))
	}

	// Make sure the deployments are rolled
	err = r.finishGitlabDeployment(gitlab)
	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error finishing GitLab deployments: %s", err))
	}

	log.Print("Running post migrations")
	migrationsJobName, err = r.runMigrations(gitlab, "post")
	r.cleanUpMigrations(gitlab, migrationsJobName)
	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error running post migrations: %s", err))
	}

	log.Print("Rolling gitlab")
	err = r.rollingUpdateGitLab(gitlab)
	if err != nil {
		return r.fail(gitlab, "Error", fmt.Sprintf("Error rolling GitLab: %s", err))
	}

	log.Print("End of reconcile run")
	err = r.markGitLabDeployed(gitlab)
	if err != nil {
		log.Printf("Error: %s", err)
		return reconcile.Result{}, err
	}

	return reconcile.Result{}, nil
}

// unpauseWorkloads unpauses all GitLab related deployments, jobs, statefulsets
// and daemonsets
func (r *ReconcileGitLab) pauseWorkloads(gitlab *gitlabv1beta1.GitLab, pause bool) error {

	for _, labelSelector := range []string{
		sidekiqLabelSelector,
		taskRunnerLabelSelector,
		webserviceLabelSelector} {
		err := r.pauseDeployments(gitlab, labelSelector, pause)
		if err != nil {
			return err
		}
	}

	for _, labelSelector := range []string{
		sharedSecretsLabelSelector,
		migrationsLabelSelector} {
		err := r.pauseJobs(gitlab, labelSelector, pause)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *ReconcileGitLab) rollingUpdateGitLab(gitlab *gitlabv1beta1.GitLab) error {

	// Sidekiq
	query := fmt.Sprintf("%s, %s=%s", sidekiqLabelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return err
	}
	deployments := &appsv1.DeploymentList{}
	err = r.List(context.TODO(), deployments, &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector})
	if err != nil {
		return err
	}
	for _, deployment := range deployments.Items {
		deployment.Spec.Template.ObjectMeta.Annotations[gitlabLastRestartAnnotationKey] = time.Now().Format(timeFormat)
		err = r.Update(context.TODO(), &deployment)
		if err != nil {
			return err
		}
	}

	// Webservice
	query = fmt.Sprintf("%s, %s=%s", webserviceLabelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err = labels.Parse(query)
	if err != nil {
		return err
	}
	err = r.List(context.TODO(), deployments, &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector})
	if err != nil {
		return err
	}
	for _, deployment := range deployments.Items {
		deployment.Spec.Template.ObjectMeta.Annotations[gitlabLastRestartAnnotationKey] = time.Now().Format(timeFormat)
		err = r.Update(context.TODO(), &deployment)
		if err != nil {
			return err
		}
	}

	// Make sure the deployments are rolled
	err = r.finishGitlabDeployment(gitlab)
	if err != nil {
		return err
	}

	return nil
}

func (r *ReconcileGitLab) cleanUpJob(gitlab *gitlabv1beta1.GitLab, jobName string) error {
	query := fmt.Sprintf("job-name=%s", jobName)
	selector, err := labels.Parse(query)

	if err != nil {
		return err
	}

	job := &batchv1.Job{}
	err = r.Get(context.TODO(), types.NamespacedName{Name: jobName, Namespace: gitlab.Namespace}, job)
	if err != nil {
		return err
	}
	err = r.Delete(context.TODO(), job)
	if err != nil {
		return err
	}
	// delete pods

	pods := &v1.PodList{}
	err = r.List(context.TODO(), pods, &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector})

	if err != nil {
		return err
	}

	for _, pod := range pods.Items {
		err = r.Delete(context.TODO(), &pod)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *ReconcileGitLab) cleanUpMigrations(gitlab *gitlabv1beta1.GitLab, jobName string) {
	err := r.cleanUpJob(gitlab, jobName)
	if err != nil {
		log.Printf("Warning: Can not delete migrations job %s", err)
	}
}

// TODO: Requires major clean up
func (r *ReconcileGitLab) cleanUpSharedSecrets(gitlab *gitlabv1beta1.GitLab, sharedSecretsJobName, selfSignedCertJobName string) {
	rbacName := fmt.Sprintf("%s-%s", gitlab.Spec.HelmRelease, "shared-secrets")

	err := r.cleanUpJob(gitlab, sharedSecretsJobName)
	if err != nil {
		log.Printf("Warning: Can not clean up job %s ", err)
	}

	if selfSignedCertJobName != "" {
		err = r.cleanUpJob(gitlab, selfSignedCertJobName)
		if err != nil {
			log.Printf("Warning: Can not clean up job %s ", err)
		}
	}

	serviceAccount := &v1.ServiceAccount{}
	role := &rbacv1.Role{}
	roleBinding := &rbacv1.RoleBinding{}

	err = r.Get(context.TODO(), types.NamespacedName{Name: rbacName, Namespace: gitlab.Namespace}, serviceAccount)
	if err != nil {
		log.Printf("Warning: Can not get service account %s ", err)
	}

	err = r.Delete(context.TODO(), serviceAccount)
	if err != nil {
		log.Printf("Warning: Can not delete service account %s ", err)
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: rbacName, Namespace: gitlab.Namespace}, role)
	if err != nil {
		log.Printf("Warning: Can not get role %s ", err)
	}

	err = r.Delete(context.TODO(), role)
	if err != nil {
		log.Printf("Warning: Can not delete role %s ", err)
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: rbacName, Namespace: gitlab.Namespace}, roleBinding)
	if err != nil {
		log.Printf("Warning: Can not get role binding %s ", err)
	}

	err = r.Delete(context.TODO(), roleBinding)
	if err != nil {
		log.Printf("Warning: Can not delete role binding %s ", err)
	}

}

func (r *ReconcileGitLab) runMigrations(gitlab *gitlabv1beta1.GitLab, migType string) (string, error) {

	log.Printf("Checking for existing migration jobs before launching a new one")
	for r.findRunningMigrations(gitlab) {
		log.Printf("Existing migrations are still running, waiting...")
		time.Sleep(5 * time.Second)
	}

	migrationsJobName := fmt.Sprintf("%s.%s.%s.%s", "migrations", gitlab.Spec.Version, migType, time.Now().Format(timeFormat))
	migrationsJobName = fmt.Sprintf("%s.%s", truncate(gitlab.Spec.HelmRelease, 63-len(migrationsJobName)-1), migrationsJobName)

	configMap := &v1.ConfigMap{}

	err := r.Get(context.TODO(), types.NamespacedName{Name: gitlab.Spec.Templates.MigrationsTemplate.ConfigMapName, Namespace: gitlab.Namespace}, configMap)

	if err != nil {
		return migrationsJobName, err
	}

	job := &batchv1.Job{}
	jsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.MigrationsTemplate.ConfigMapKey]))

	if err != nil {
		return migrationsJobName, err
	}

	err = json.Unmarshal(jsonData, job)

	if err != nil {
		return migrationsJobName, err
	}

	migrationsJob := formatMigrationsJob(gitlab, job, migrationsJobName, migType)

	err = r.Create(context.TODO(), migrationsJob)
	if err != nil {
		return migrationsJobName, err
	}

	log.Printf("%s launched, waiting for it to appear", migrationsJobName)

	// Run with timeout. The job may not appear immediately. If the job does not
	// appear after 300 seconds, we will error
	timeout := 0
	for {
		job := &batchv1.Job{}
		err := r.Get(context.TODO(), types.NamespacedName{Name: migrationsJobName, Namespace: gitlab.Namespace}, job)

		if err != nil {
			if timeout < 300 {
				log.Printf("Received an error while waiting for %s to appear: %s", migrationsJobName, err)
				log.Printf("Will continue to wait for %d more seconds", 300-timeout)
				timeout += 5
				time.Sleep(5 * time.Second)
				continue
			} else {
				return migrationsJobName, fmt.Errorf("Timed out waiting for %s to appear: %s", migrationsJobName, err)
			}
		}

		if job.Status.Succeeded == 1 || job.Status.Failed == 1 {
			break
		}

		log.Printf("%s is still running, waiting until it completes", migrationsJobName)
		time.Sleep(5 * time.Second)
	}

	log.Printf("Migrations completed")
	return migrationsJobName, nil
}

func (r *ReconcileGitLab) findRunningMigrations(gitlab *gitlabv1beta1.GitLab) bool {
	jobs := &batchv1.JobList{}
	migTypes := []string{"pre", "post"}
	for _, migType := range migTypes {
		query := fmt.Sprintf("migrationType=%s,%s=%s", migType, helmLabelSelector, gitlab.Spec.HelmRelease)
		selector, err := labels.Parse(query)
		if err != nil {
			log.Printf("Error parsing %s: %s", query, err.Error())
			return true
		}

		err = r.List(context.TODO(), jobs, &client.ListOptions{
			LabelSelector: selector,
			Namespace:     gitlab.Namespace,
		})
		if err != nil {
			log.Printf("Error looking for jobs: %s", err)
			// return true as we don't want to launch new jobs if we can't determine if old ones
			// are still running
			return true
		}

		for _, job := range jobs.Items {
			if job.Status.Succeeded == 0 && job.Status.Failed == 0 {
				log.Printf("%s is still running", job.Name)
				return true
			}
		}
	}

	log.Printf("No migration jobs currently running")
	return false
}

// TODO: requires major refactoring and cleanup specifically in the way the rbac is handled
// reflections can probably be used here to DRY up stuff. Also parsing can become a single json.UnMarshal
func (r *ReconcileGitLab) runSharedSecrets(gitlab *gitlabv1beta1.GitLab) (string, string, error) {

	sharedSecretsJobName := fmt.Sprintf("%s.%s.%s", "shared-secrets", gitlab.Spec.Version, time.Now().Format(timeFormat))
	sharedSecretsJobName = fmt.Sprintf("%s.%s", truncate(gitlab.Spec.HelmRelease, 63-len(sharedSecretsJobName)-1), sharedSecretsJobName)

	selfSignedCertJobName := fmt.Sprintf("%s.%s.%s", "self-signed-certs", gitlab.Spec.Version, time.Now().Format(timeFormat))
	selfSignedCertJobName = fmt.Sprintf("%s.%s", truncate(gitlab.Spec.HelmRelease, 63-len(selfSignedCertJobName)-1), selfSignedCertJobName)

	configMap := &v1.ConfigMap{}

	err := r.Get(context.TODO(), types.NamespacedName{Name: gitlab.Spec.Templates.SharedSecretsTemplate.ConfigMapName, Namespace: gitlab.Namespace}, configMap)

	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}

	// We need to create rbac dependencies

	serviceAccount := &v1.ServiceAccount{}
	role := &rbacv1.Role{}
	roleBinding := &rbacv1.RoleBinding{}
	job := &batchv1.Job{}

	var selfSignedJsonData []byte
	selfSignedJob := &batchv1.Job{}

	if configMap.Data["selfSignedCertTemplate"] != "" {
		selfSignedJsonData, err = yaml.YAMLToJSON([]byte(configMap.Data["selfSignedCertTemplate"]))
		if err != nil {
			return sharedSecretsJobName, selfSignedCertJobName, err
		}
		err = json.Unmarshal(selfSignedJsonData, &selfSignedCertJobName)
		if err != nil {
			return sharedSecretsJobName, selfSignedCertJobName, err
		}
	} else {
		selfSignedCertJobName = ""
	}

	serviceAccountJsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.SharedSecretsTemplate.ServiceAccountKey]))
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}

	roleJsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.SharedSecretsTemplate.RoleKey]))
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}

	roleBindingJsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.SharedSecretsTemplate.RoleBindingKey]))
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}

	jobJsonData, err := yaml.YAMLToJSON([]byte(configMap.Data[gitlab.Spec.Templates.SharedSecretsTemplate.ConfigMapKey]))
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}

	err = json.Unmarshal(serviceAccountJsonData, serviceAccount)
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}
	serviceAccount.ObjectMeta.Namespace = gitlab.Namespace

	err = json.Unmarshal(roleJsonData, role)
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}
	role.ObjectMeta.Namespace = gitlab.Namespace

	err = json.Unmarshal(roleBindingJsonData, roleBinding)
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}
	roleBinding.ObjectMeta.Namespace = gitlab.Namespace

	err = json.Unmarshal(jobJsonData, job)
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: serviceAccount.ObjectMeta.Name, Namespace: gitlab.Namespace}, &v1.ServiceAccount{})
	if err != nil {
		err = r.Create(context.TODO(), serviceAccount)
		if err != nil {
			return sharedSecretsJobName, selfSignedCertJobName, err
		}
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: role.ObjectMeta.Name, Namespace: gitlab.Namespace}, &rbacv1.Role{})
	if err != nil {
		err = r.Create(context.TODO(), role)
		if err != nil {
			return sharedSecretsJobName, selfSignedCertJobName, err
		}
	}

	err = r.Get(context.TODO(), types.NamespacedName{Name: roleBinding.ObjectMeta.Name, Namespace: gitlab.Namespace}, &rbacv1.RoleBinding{})
	if err != nil {
		err = r.Create(context.TODO(), roleBinding)
		if err != nil {
			return sharedSecretsJobName, selfSignedCertJobName, err
		}
	}

	sharedSecretsJob := formatSharedSecretsJob(gitlab, job, sharedSecretsJobName)

	err = r.Create(context.TODO(), sharedSecretsJob)
	if err != nil {
		return sharedSecretsJobName, selfSignedCertJobName, err
	}

	if configMap.Data["selfSignedCertTemplate"] != "" {
		selfSignedJob = formatSharedSecretsJob(gitlab, selfSignedJob, selfSignedCertJobName)

		err = r.Create(context.TODO(), selfSignedJob)
		if err != nil {
			return sharedSecretsJobName, selfSignedCertJobName, err
		}
	}

	// Now we should wait for the job to finish excution!
	timeout := 0
	success := false
	for {
		if timeout > 300 {
			log.Println("shared-secrets Job didn't finish in 5 minutes. Backing off")
			break
		}

		job := &batchv1.Job{}
		err := r.Get(context.TODO(), types.NamespacedName{Name: sharedSecretsJobName, Namespace: gitlab.Namespace}, job)

		if err != nil {
			continue
		}

		if job.Status.Succeeded == 1 {
			success = true
			break
		}

		if job.Status.Failed == 1 {
			break
		}

		timeout += 5
		time.Sleep(5 * time.Second)
	}

	if !success {
		return sharedSecretsJobName, selfSignedCertJobName, fmt.Errorf("Shared secrets job %s failed", sharedSecretsJobName)
	}

	log.Print("Shared secrets job completed")
	return sharedSecretsJobName, selfSignedCertJobName, nil
}

func (r *ReconcileGitLab) pauseJobs(gitlab *gitlabv1beta1.GitLab, labelSelector string, pause bool) error {

	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return err
	}

	jobs := &batchv1.JobList{}
	err = r.List(context.TODO(), jobs, &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector})
	if err != nil {
		return err
	}

	parallelism := int32Pointer(defaultJobParallelism)
	if pause {
		parallelism = int32Pointer(0)
	}

	for _, job := range jobs.Items {
		job.Spec.Parallelism = parallelism

		err = r.Update(context.TODO(), &job)
		if err != nil {
			log.Printf("Error updating job %s: %s", job.Name, err.Error())
			return err
		}
	}

	return nil
}

func (r *ReconcileGitLab) pauseDeployments(gitlab *gitlabv1beta1.GitLab, labelSelector string, pause bool) error {
	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		log.Printf("Error parsing %s: %s", query, err.Error())
		return err
	}

	deployments := &appsv1.DeploymentList{}
	err = r.List(context.TODO(), deployments, &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector})
	if err != nil {
		log.Printf("Error listing deployments: %s", err.Error())
		return err
	}

	specPaused := false
	if pause {
		specPaused = true
	}

	for _, deployment := range deployments.Items {
		deployment.Spec.Paused = specPaused

		err = r.Update(context.TODO(), &deployment)
		if err != nil {
			log.Printf("Error updating deployment %s: %s", deployment.Name, err.Error())
			return err
		}
	}
	return nil
}

func (r *ReconcileGitLab) pauseStatefulSets(gitlab *gitlabv1beta1.GitLab, labelSelector string, pause bool) error {

	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return err
	}

	statefulSets := &appsv1.StatefulSetList{}
	err = r.List(context.TODO(), statefulSets, &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector})
	if err != nil {
		return err
	}

	for _, statefulSet := range statefulSets.Items {
		if pause {
			log.Printf("Pausing statefulSet %s", statefulSet.Name)
			statefulSet.Spec.UpdateStrategy.RollingUpdate.Partition = statefulSet.Spec.Replicas
		} else {
			log.Printf("Unpausing statefulSet %s", statefulSet.Name)
			statefulSet.Spec.UpdateStrategy.RollingUpdate.Partition = int32Pointer(0)
		}
		err = r.Update(context.TODO(), &statefulSet)
		if err != nil {
			return err
		}
	}

	return nil
}

// Based off completeRollingUpdate from: https://github.com/kubernetes/kubernetes/blob/2e6b073a3f800654ec217e763fcb97412308a9db/pkg/controller/statefulset/stateful_set_utils.go#L382
func checkRollingUpdate(set *appsv1.StatefulSet, status *appsv1.StatefulSetStatus) bool {
	return set.Spec.UpdateStrategy.Type == appsv1.RollingUpdateStatefulSetStrategyType &&
		status.UpdatedReplicas == status.Replicas &&
		status.ReadyReplicas == status.Replicas &&
		status.CurrentRevision == status.UpdateRevision
}

func (r *ReconcileGitLab) checkStatefulSetFinished(gitlab *gitlabv1beta1.GitLab, labelSelector string) (bool, error) {
	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return false, err
	}

	statefulSets := &appsv1.StatefulSetList{}
	err = r.List(context.TODO(), statefulSets, &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector})
	if err != nil {
		return false, err
	}
	for _, statefulSet := range statefulSets.Items {
		if !checkRollingUpdate(&statefulSet, &statefulSet.Status) {
			log.Printf("StatefulSet is still updating for %s", statefulSet.Name)
			return false, nil
		}
		log.Printf("StatefulSet is updated for %s", statefulSet.Name)
	}
	return true, nil
}

func (r *ReconcileGitLab) checkDeploymentFinished(gitlab *gitlabv1beta1.GitLab, labelSelector string) (bool, error) {
	query := fmt.Sprintf("%s, %s=%s", labelSelector, helmLabelSelector, gitlab.Spec.HelmRelease)
	selector, err := labels.Parse(query)
	if err != nil {
		return false, err
	}

	deployments := &appsv1.DeploymentList{}
	err = r.List(context.TODO(), deployments, &client.ListOptions{Namespace: gitlab.Namespace, LabelSelector: selector})
	if err != nil {
		return false, err
	}
	for _, deployment := range deployments.Items {
		if !deploymentComplete(&deployment, &deployment.Status) {
			log.Printf("Deployment is still updating for %s", deployment.Name)
			return false, nil
		}
	}

	return true, nil
}

// Same check as used in the deployment utils in upstream Kubernetes
// https://github.com/kubernetes/kubernetes/blob/master/pkg/controller/deployment/util/deployment_util.go#L722
func deploymentComplete(deployment *appsv1.Deployment, newStatus *appsv1.DeploymentStatus) bool {
	return newStatus.UpdatedReplicas == *(deployment.Spec.Replicas) &&
		newStatus.Replicas == *(deployment.Spec.Replicas) &&
		newStatus.AvailableReplicas == *(deployment.Spec.Replicas) &&
		newStatus.ObservedGeneration >= deployment.Generation
}

func (r *ReconcileGitLab) finishGitlabDeployment(gitlab *gitlabv1beta1.GitLab) error {
	// We have to wait for some time, as it is possible that update call is a bit delayed.
	log.Printf("Waiting for 5 seconds to start checking for status of Gitlab deployments")
	time.Sleep(5 * time.Second)

	timeout := 0
	for {
		if timeout > 300 {
			return fmt.Errorf("timeout was longer then 300")
		}
		deployed := false
		// Sidekiq
		deployed, err := r.checkDeploymentFinished(gitlab, sidekiqLabelSelector)
		if err != nil {
			log.Printf("Ran into error but will try to continue: %v", err)
			continue
		}

		// Webservice
		deployed, err = r.checkDeploymentFinished(gitlab, webserviceLabelSelector)

		if err != nil {
			log.Printf("Ran into error but will try to continue: %v", err)
			continue
		}

		if deployed {
			log.Printf("Sidekiq and Webservice are rolled")
			break
		} else {
			timeout += 5
			time.Sleep(5 * time.Second)
		}
	}

	return nil
}

func (r *ReconcileGitLab) markGitLabDeployed(gitlab *gitlabv1beta1.GitLab) error {
	//  Clear out any previous failure
	r.removeCondition(gitlab, gitlabv1beta1.ConditionFailure)
	// Recored the revision if available
	if gitlab.Spec.Revision != "" {
		gitlab.Status.DeployedRevision = gitlab.Spec.Revision
	}
	return r.updateGitLabStatus(gitlab)
}

func (r *ReconcileGitLab) updateGitLabStatus(gitlab *gitlabv1beta1.GitLab) error {
	gitlab.Status.ObservedGeneration = gitlab.ObjectMeta.Generation

	err := r.Status().Update(context.Background(), gitlab)
	if err != nil {
		return err
	}

	return nil
}

// fail - helper function to set fail condition with reason and message
func (r *ReconcileGitLab) fail(gitlab *gitlabv1beta1.GitLab, reason, msg string) (reconcile.Result, error) {
	r.setCondition(gitlab, *r.newCondition(gitlabv1beta1.ConditionFailure, v1.ConditionTrue, reason, msg))
	return reconcile.Result{Requeue: true}, r.updateGitLabStatus(gitlab)
}

func (r *ReconcileGitLab) newCondition(condType gitlabv1beta1.ConditionType, status v1.ConditionStatus, reason, message string) *gitlabv1beta1.Condition {
	return &gitlabv1beta1.Condition{
		Type:               condType,
		Status:             status,
		LastTransitionTime: metav1.Now(),
		Reason:             reason,
		Message:            message,
	}
}

func (r *ReconcileGitLab) getCondition(gitlab gitlabv1beta1.GitLab, condType gitlabv1beta1.ConditionType) *gitlabv1beta1.Condition {
	for i := range gitlab.Status.Conditions {
		c := gitlab.Status.Conditions[i]
		if c.Type == condType {
			return &c
		}
	}
	return nil
}

func (r *ReconcileGitLab) setCondition(gitlab *gitlabv1beta1.GitLab, condition gitlabv1beta1.Condition) {
	currentCond := r.getCondition(*gitlab, condition.Type)
	if currentCond != nil && currentCond.Status == condition.Status && currentCond.Reason == condition.Reason {
		return
	}
	// Do not update lastTransitionTime if the status of the condition doesn't change.
	if currentCond != nil && currentCond.Status == condition.Status {
		condition.LastTransitionTime = currentCond.LastTransitionTime
	}
	newConditions := filterOutCondition(gitlab.Status.Conditions, condition.Type)
	gitlab.Status.Conditions = append(newConditions, condition)
}

func (r *ReconcileGitLab) removeCondition(gitlab *gitlabv1beta1.GitLab, condType gitlabv1beta1.ConditionType) {
	gitlab.Status.Conditions = filterOutCondition(gitlab.Status.Conditions, condType)
}

func filterOutCondition(conditions []gitlabv1beta1.Condition, condType gitlabv1beta1.ConditionType) []gitlabv1beta1.Condition {
	var newConditions []gitlabv1beta1.Condition
	for _, c := range conditions {
		if c.Type == condType {
			continue
		}
		newConditions = append(newConditions, c)
	}
	return newConditions
}

func int32Pointer(x int32) *int32 {
	return &x
}

func int64Pointer(x int64) *int64 {
	return &x
}

func (r *ReconcileGitLab) rollingUpdateGitaly(gitlab *gitlabv1beta1.GitLab) error {

	// Unpause Gitaly
	err := r.pauseStatefulSets(gitlab, gitalyLabelSelector, false)
	if err != nil {
		return err
	}

	// Wait for Gitaly to finish rolling update
	finishedChannel := make(chan error, 0)
	go func() {
		for {
			deployed, err := r.checkStatefulSetFinished(gitlab, gitalyLabelSelector)
			if err != nil {
				finishedChannel <- err
				return
			}
			if deployed != true {
				time.Sleep(gitalyRollingUpdateCheckPeriod)
				continue
			}
			finishedChannel <- nil
		}
	}()

	select {
	case err := <-finishedChannel:
		if err != nil {
			return err
		}
		log.Printf("Gitaly is rolled")
	case <-time.After(gitalyRollingUpdateTimeout):
		return fmt.Errorf("timeout waiting for Gitaly to roll")
	}

	return nil
}

func truncate(str string, n int) string {
	if len(str) > n {
		return str[:n]
	}

	return str
}
