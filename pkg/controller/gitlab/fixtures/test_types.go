/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package fixtures

import (
	"gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/apis"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/runtime/scheme"
)

// GitLab type for test
type GitLab struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
}

// GitLabList type for test
type GitLabList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []GitLab `json:"items"`
}

// Test type registeration (must be called the first thing in a test suite)
func RegisterTestTypes(group string, version string) {
	schemaBuilder := &scheme.Builder{
		GroupVersion: schema.GroupVersion{
			Group:   group,
			Version: version,
		},
	}
	schemaBuilder.Register(&GitLab{}, &GitLabList{})
	apis.AddToSchemes = append(apis.AddToSchemes, schemaBuilder.AddToScheme)

}

func (in *GitLab) DeepCopyInto(out *GitLab) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	return
}

func (in *GitLab) DeepCopy() *GitLab {
	if in == nil {
		return nil
	}
	out := new(GitLab)
	in.DeepCopyInto(out)
	return out
}

func (in *GitLab) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

func (in *GitLabList) DeepCopyInto(out *GitLabList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	out.ListMeta = in.ListMeta
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]GitLab, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	return
}

func (in *GitLabList) DeepCopy() *GitLabList {
	if in == nil {
		return nil
	}
	out := new(GitLabList)
	in.DeepCopyInto(out)
	return out
}

func (in *GitLabList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}
