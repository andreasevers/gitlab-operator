package fixtures

import (
	"context"
	"sync"
	"time"

	"github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var gracePeriodSeconds int64 = 0
var propagationPolicy metav1.DeletionPropagation = metav1.DeletePropagationBackground

type NewReconcilerFunc = func(m manager.Manager) reconcile.Reconciler

type ManagerStopFunc = func()

type TestSetup struct {
	Manager manager.Manager
	Stop    ManagerStopFunc
	g       *gomega.GomegaWithT
}

func (s *TestSetup) GetClient() client.Client {
	s.g.Expect(s.Manager).NotTo(gomega.BeNil())
	return s.Manager.GetClient()
}

func (s *TestSetup) GetScheme() *runtime.Scheme {
	s.g.Expect(s.Manager).NotTo(gomega.BeNil())
	return s.Manager.GetScheme()
}

func (s *TestSetup) Create(obj runtime.Object) error {
	return s.GetClient().Create(context.TODO(), obj)
}

func (s *TestSetup) Delete(obj runtime.Object) error {

	// A small delay to make sure cache picks up the change.
	// Should be replace with something more coherent.
	defer time.Sleep(1 * time.Second)
	return s.GetClient().Delete(context.TODO(), obj, &client.DeleteOptions{
		GracePeriodSeconds: &gracePeriodSeconds,
		PropagationPolicy:  &propagationPolicy,
	})
}

func (s *TestSetup) Get(key metav1.ObjectMeta, obj runtime.Object) error {
	return s.GetClient().Get(context.TODO(), client.ObjectKey{
		Name:      key.Name,
		Namespace: key.Namespace,
	}, obj)
}

func (s *TestSetup) UpdateStatus(obj runtime.Object) error {
	return s.GetClient().Status().Update(context.TODO(), obj)
}

func (s *TestSetup) List(list runtime.Object, opts *client.ListOptions) error {
	return s.GetClient().List(context.TODO(), list, opts)
}

func (s *TestSetup) WrapReconciler(newRecFunc NewReconcilerFunc) (reconcile.Reconciler, chan reconcile.Request) {
	original := newRecFunc(s.Manager)
	requests := make(chan reconcile.Request)
	delegate := reconcile.Func(func(req reconcile.Request) (reconcile.Result, error) {
		result, err := original.Reconcile(req)
		requests <- req
		return result, err
	})
	return delegate, requests
}

func NewTestSetup(c *rest.Config, g *gomega.GomegaWithT) *TestSetup {
	manager, err := manager.New(c, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())

	stopManager := make(chan struct{})
	managerStopped := &sync.WaitGroup{}
	go func() {
		managerStopped.Add(1)
		g.Expect(manager.Start(stopManager)).NotTo(gomega.HaveOccurred())
		managerStopped.Done()
	}()

	return &TestSetup{
		Manager: manager,
		Stop: func() {
			close(stopManager)
			managerStopped.Wait()
		},
		g: g,
	}
}
